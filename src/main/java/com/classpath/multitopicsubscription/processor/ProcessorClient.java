package com.classpath.multitopicsubscription.processor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.function.Consumer;

@Configuration
public class ProcessorClient {

    @Bean
    public Consumer<String> processMessage(){
        return (message) -> System.out.println(message.toLowerCase());
    }

    @Bean
    public Consumer<String> processAllMessage(){
        return (message) -> System.out.println(message.toUpperCase());
    }
}